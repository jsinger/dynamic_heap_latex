#!/usr/bin/env python
# Turn a CSV results file from timer comparison many into a latex table

import csv
import sys

inFile = "results.csv"

class CircularList(object):
    def __init__(self, l):
        super(object, self)
        self.l = l
        self.i = 0

    def __iter__(self):
        return self

    def next(self):
        item = self.l[self.i]
        self.i = (self.i + 1) % len(self.l)
        return item

def fix_val(s):
    s = s.strip("\"")
    a = s.split(" ")
    return "$%.2f \\pm{} %.2f$" % (float(a[0]), float(a[2]))

def fix_bms(bms):
    a = bms.split("|")
    return (fix_bm(a[0]), fix_bm(a[1]))

def fix_bm(bm):
    return bm.replace(":", ", ")

factors = CircularList(["1.1", "1.3", "1.5", "1.7", "1.9"])

with open(inFile) as f, open("highestTotalMem.txt") as highest, open("whichCrash.txt") as crash:
    data = csv.DictReader(f)
    sys.stdout.write("\\begin{tabular}{cc|ccc|ccc|c}\n")
    sys.stdout.write("Benchmark 1 & Benchmark 2 & Budget & Budget & Budget & Unconstrained & Static equal & Daemon & Daemon max \\\\\n")
    sys.stdout.write("\\multicolumn{2}{c|}{(name, size, threads, iterations)} & factor & (MB) & $\\times 2$ (MB) & runtime (s) & runtime (s) & runtime (s) & heap (MB) \\\\ \\hline\n")
    for line in data:
        (bm1, bm2) = fix_bms(line["BENCHMARKS"])
        factor = factors.next()
        heap = line["MAX_HEAP"]
        unconstrained = fix_val(line["UNCONSTRAINED"])
        static = fix_val(line["STATIC_EQUAL"])
        daemon = fix_val(line["DAEMON_VENGEROV"])
        max_total = highest.readline().strip()
        crashes = crash.readline().strip()
        sys.stdout.write(" & ".join([bm1, bm2, factor, heap, str(int(heap) * 2), unconstrained, "-" if crashes else static, daemon, max_total]) + " \\\\")
        if factor == "1.9":
            sys.stdout.write(" \\hline")
        sys.stdout.write("\n")
    sys.stdout.write("\\end{tabular}\n")
