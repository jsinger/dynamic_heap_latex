#!/usr/bin/env python

import matplotlib.pyplot as plt
from mpl_toolkits.axes_grid.axislines import SubplotZero

fig = plt.figure()
ax = SubplotZero(fig, 111)
fig.add_subplot(ax)

for direction in ["xzero", "yzero"]:
    ax.axis[direction].set_axisline_style("-|>")
    ax.axis[direction].set_visible(True)

for direction in ["left", "right", "bottom", "top"]:
    ax.axis[direction].set_visible(False)

def U(x):
    return x ** 0.5

x = range(1, 200)
y = map(U, x)

ax.plot(x, y)

ax.annotate("Heap size (h)", (105, -0.75), annotation_clip=False, ha="center", va="center", size="large")
ax.annotate("Throughput (T)", (-8, 8), annotation_clip=False, ha="center", va="center", rotation=90, size="large")
ax.set_xticks([])
ax.set_yticks([])
ax.set_xbound(0, 210)
ax.set_ybound(0, 16)

plt.savefig("generated/singleThroughput.pdf")
