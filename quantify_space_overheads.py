#!/usr/bin/python 

# quant_overheads.py based on gen_results_graph.py
# Jeremy Singer
# 2 Apr 15

# After ISMM review, our paper is subject to shepherding
# (i.e. it has been conditionally accepted)
# I need to report relative overheads of Forseti 
# vs baseline
# as advised by shepherd.

import warnings

import numpy as np
import re


# table of results from paper - 
# alternatively use results.csv (same data, diff format)
resultsFile = "generated/slim_resultsTable.tex"

# expts are for minheap x {1.1,1.3,1.5,1.7,1.9}
heapsizes = np.arange(1.1, 2.0,0.2)

# store results for expt timings (parameterize on heap size)
# effectively, each of the Times structures below is a dictionary of arrays
# (index on heap size)
targetHeaps = []
daemonHeaps = []
overheads = []

# read results file line-by-line
for line in open(resultsFile):
    if '\\' in line:
        data = line.split('&')
        if len(data) < 4:
            continue
        target = data[len(data)-2]
        try:
            targetHeaps.append(int(target))
            daemonHeaps.append(int(data[-1].split()[0]))
            
        except ValueError:
            # table headings, not numbers
            continue

# compare forseti overhead with static equal
overheads = []    
for i in range(len(targetHeaps)):
    overheads.append(float(daemonHeaps[i])/targetHeaps[i])
    #print i, ",", targetHeaps[i], ",", daemonHeaps[i]

print "N is", len(overheads), ", mean forseti overhead cf target:", np.mean(overheads)


# then  make adjustments for baseline targets (multiply by num. bms)
for i in range(len(targetHeaps)):
    if (i < len(targetHeaps)-5):
        targetHeaps[i] = targetHeaps[i]*2
    else:
        targetHeaps[i] = targetHeaps[i]*3

overheads = []    
for i in range(len(targetHeaps)):
    overheads.append(float(daemonHeaps[i])/targetHeaps[i])
    #print i, ",", targetHeaps[i], ",", daemonHeaps[i]

print "N is", len(overheads), ", mean forseti space saving cf baseline:", np.mean(overheads)
