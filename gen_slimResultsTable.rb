#!/usr/bin/ruby

resultsFile = 'resultsTable.tex'

#puts "reading #{resultsFile}"

puts '\\begin{tabular}{cc|rr|r}'

File.open(resultsFile).each do |line|
  # for ordinary data row, we need
  # columns 0,1,2,3 and last
  data = line.split(/&/)
  bm1 = ""
  bm2 = ""
  if line.match('multicolumn\{2\}') then
    isHeading = false
    bm = data[0]
    factor = data[1]
    if factor.match('factor') then
      isHeading = true
    end
    if data[1].match(/[0-9]/) then
      factor = data[1].to_f
      isHeading = false
    end
    targetHeapSize = data[2]
    if data[2].match(/[0-9]/) then
      targetHeapSize = data[2].to_i
      isHeading = false
    end
    daemonMaxHeap = data.last.split(/\\/).first
    if daemonMaxHeap.match(/[0-9]/) then
       daemonMaxHeap = daemonMaxHeap.to_i
    end
    print "#{bm} & #{factor} & #{targetHeapSize} & #{daemonMaxHeap} \\\\"
    if (isHeading) then
      print " \\hline"
    end
    puts ""
    next
  end

  if line.match('multirow\{5\}') then
    bm1 = data[0]
    bm2 = data[1]
  end

  if line.match('\\pm\{\}') then
    factor = data[2].to_f
    targetHeapSize = data[3].to_i
    daemonMaxHeap = data.last.split(/\\/).first.to_i
    print "#{bm1} & #{bm2} & #{factor} & #{targetHeapSize} & #{daemonMaxHeap} \\\\"
    if factor == 1.9 then
      print " \\hline"
    end
    puts ""
  end
  

end

puts '\\end{tabular}'
