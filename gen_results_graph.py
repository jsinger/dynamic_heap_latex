#!/usr/bin/python 

# gen_results_graph.py
# Jeremy Singer
# 2 Apr 15

# After ISMM review, our paper is subject to shepherding
# (i.e. it has been conditionally accepted)
# I need to change the resultsTable.tex into a graph, 
# as advised by shepherd.

import warnings

import matplotlib.pyplot as plt
import matplotlib.lines as mlines
import numpy as np
import re
from pylab import rcParams
rcParams['figure.figsize'] = 7, 10

# table of results from paper - 
# alternatively use results.csv (same data, diff format)
resultsFile = "resultsTable.tex"

# expts are for minheap x {1.1,1.3,1.5,1.7,1.9}
heapsizes = np.arange(1.1, 2.0,0.2)

# store results for expt timings (parameterize on heap size)
# effectively, each of the Times structures below is a dictionary of arrays
# (index on heap size)
baselineTimes = {}
staticTimes = {}
staticSDs = {}
forsetiTimes = {}
forsetiSDs = {}

# parse .tex tabular to get out a single timing result
def getTime(line, columnIndex):
    time = line.split('&')[columnIndex]
    if ('-' in time):
        # no time (this expt did not complete - crash with OOM error)
        time = 0
    else:
        # time available, need to parse to get numbers out
        time = time.split()[0]
        time = float(time[1:])
    return time

# parse .tex tabular to get out a single standard deviation
def getSD(line, columnIndex):
    time = line.split('&')[columnIndex]
    if ('-' in time):
        # no time (this expt did not complete - crash with OOM error)
        sd = 0
    else:
        # time available, need to parse to get numbers out
        sd = time.split()[2]
        sd = float(sd[:-1])
    return sd


# scrape benchmark combinations (12 of them) from results csv file
bmCombos = []
for line in open('results.csv'):
    bmCombo = line.split(',')[0]
    if ('|' in bmCombo) and (not (bmCombo in bmCombos)):
        bmCombos.append(bmCombo)

# correction for final 3-way combo
bmCombos.append('h2:large:2:13|jython:large:1:28|sunflow:large:2:32')

print bmCombos
    
# scrape times from results table
for heapsize in heapsizes:
    baselineTimes[heapsize] = []
    staticTimes[heapsize] = []
    staticSDs[heapsize] = []
    forsetiTimes[heapsize] = []
    forsetiSDs[heapsize] = []
    # read results file line-by-line
    for line in open(resultsFile):
        if (' ' + str(heapsize) + ' ') in line:
            #print line
            if 'multicolumn' in line:
                # correction for column index
               timeIndex = 4
            else:
               timeIndex = 5 
            time = getTime(line, timeIndex)
            sd = getSD(line, timeIndex) # not actually using baseline sd
            #print time, "+/-", sd
            baselineTimes[heapsize].append(time)
            time = getTime(line, timeIndex+1)
            sd = getSD(line, timeIndex+1)
            staticTimes[heapsize].append(time)
            staticSDs[heapsize].append(sd)
            time = getTime(line, timeIndex+2)
            sd = getSD(line, timeIndex+2)
            forsetiTimes[heapsize].append(time)
            forsetiSDs[heapsize].append(sd)

# for heapsize in heapsizes:
#     print heapsize, ":", times[heapsize]

numCols = 3
numRows = 4
textSize = 12


fig, axes = plt.subplots(nrows=numRows, ncols=numCols, sharex=False)
expt = 0
for ax in axes.flat:
    print expt
    staticResultsTimes = []
    staticResultsSDs = []
    forsetiResultsTimes = []
    forsetiResultsSDs = []
    for heapsize in heapsizes:
        staticResultsTimes.append(staticTimes[heapsize][expt] / baselineTimes[heapsize][expt])
        staticResultsSDs.append(staticSDs[heapsize][expt] / baselineTimes[heapsize][expt])
        forsetiResultsTimes.append(forsetiTimes[heapsize][expt] / baselineTimes[heapsize][expt])
        forsetiResultsSDs.append(forsetiSDs[heapsize][expt] / baselineTimes[heapsize][expt])
        

    # only pick static result times that are non-zero (0 time indicates crashed expt)
    xs = list(heapsizes)  # copy!
    while (staticResultsTimes.count(0) > 0):
        i = staticResultsTimes.index(0)
        # remove 0 time and corresponding heap size
        xs.pop(i)
        staticResultsTimes.pop(i)
        staticResultsSDs.pop(i)

# get HTML color names from http://www.w3schools.com/html/html_colornames.asp 
    if len(xs)>0:
        ax.errorbar(xs,staticResultsTimes,yerr=staticResultsSDs, marker='s', fmt='-', color="LightBlue", linewidth=3, markersize=9)
    ax.errorbar(heapsizes,forsetiResultsTimes,yerr=forsetiResultsSDs, marker='D', fmt='-', color="Navy", linewidth=3, markersize=9)
    ax.set_title(bmCombos[expt].replace('|','\n'), fontsize=textSize)
    
    # x axes
    ax.set_xlim(1.05,1.95)
    ax.set_xticks(heapsizes)

    if (expt % numCols == 0):
        ax.set_ylabel('time relative to\nunconstrained execution', fontsize=textSize, multialignment='center')
    if (expt >= (numRows-1)*(numCols)):
        ax.set_xlabel('size rel. to minheap', fontsize=textSize-1)
        
    # fewer ticks on y
    #max_yticks = 4
    #yloc = plt.MaxNLocator(max_yticks)
    #ax.yaxis.set_major_locator(yloc)   
    ax.set_ybound(lower=0.95)
    # special case
    if expt == 2 or expt == 7 or expt == 8 or expt == 9 or expt == 11:
        ax.set_ybound(lower=1.0)

    # special case 
    if expt == 5:
        ax.set_ybound(lower=1,upper=11)
        ax.set_yticks(np.arange(1,11,1))

    # special case 
    if expt == 6:
        ax.set_ybound(lower=1,upper=17)
        ax.set_yticks(np.arange(1,17,1))
    
    expt = expt + 1

static_line = mlines.Line2D([], [], color='LightBlue', marker='s',
                          linewidth=3, markersize=9, label='Static')
forseti_line = mlines.Line2D([], [], color='Navy', marker='D',
                          linewidth=3, markersize=9, label='Forseti')

axpos = ax.get_position()
fig.legend(handles=[static_line, forseti_line], labels=['Static','Forseti'], loc=(axpos.x0+0.133,axpos.y0+0.047))
#ax.legend(loc='center', borderaxespad=0.0, bbox_to_anchor = (0.15,-0.2,1,1),)

plt.tight_layout(pad=0.1, w_pad=0.2, h_pad=0.7)
    
    

plt.show()
