BASENAME = paper

$(BASENAME).pdf: generated images/* generated/singleThroughput.pdf generated/combinedThroughput.pdf generated/system.pdf resultsTable.tex generated/slim_resultsTable.tex

%.pdf: %.tex %.bib
	latexmk -pdf -file-line-error -halt-on-error $*.tex
ifeq ($(shell /bin/hostname | cut -f1  -d.), savu)
	scp $*.pdf sibu:public_html/a.pdf
endif

generated:
	mkdir -p generated

save: $(BASENAME).pdf
	mkdir -p saved
	cp -t saved $(BASENAME).pdf

clean:
	rm -rf $(BASENAME).pdf *.dvi *.out *.bbl *.aux *.log *.blg *.pyc *.fls *.fdb_latexmk *~ generated auto


generated/singleThroughput.pdf: generated
	./singleThroughput.py
	pdfcrop generated/singleThroughput.pdf generated/singleThroughput.pdf

generated/combinedThroughput.pdf: images/combinedThroughput.svg generated
	./svg2pdf images/combinedThroughput.svg  $(CURDIR)/generated/combinedThroughput.pdf
	pdfcrop generated/combinedThroughput.pdf generated/combinedThroughput.pdf

generated/system.pdf: images/system.dia generated
	./dia2pdf ./images/system.dia ./generated/system.pdf
	pdfcrop generated/system.pdf generated/system.pdf

generated/slim_resultsTable.tex: generated resultsTable.tex
	./gen_slimResultsTable.rb > ./generated/slim_resultsTable.tex